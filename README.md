# iotest.py
Test i/o using dd by doing N write/read tests from/to /dev/zero to/from $target.

Configurable modes, output, payload etc.  Also scriptable for exection via cron and logging (via redirect)

Can be deployed to multiple nodes for load testing using [iotest-cluster](https://gitlab.com/hoolio/iotest-cluster)

## Installation and quick start

If you have python and dd, it should just work already.  Installing is a git cone, and execution is simple:
```
$ git clone https://github.com/hooliowobbits/iotest.git
$ cd ./iotest/
$ ./iotest.py -d .
```
This would run 4 read/write tests of 150 mb each to the current directory and then return you some stats.  More details below.

## Target platforms
### Linuxes
Currently working wherever i've tested it, let me know if you have problems.

### OSX
Proper support is not impossible (right?), but there are a couple of issues, one fixable, one not so much yet:

* The native /bin/dd is from BSD and behaves quite differently to the Linux version, answer is to:
  * [install](https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/) [brew](https://brew.sh/) which is a package manager containing linux tools for OSX
  * then use brew to install the GNU dd binary from the coreutils package:  
```$ brew install coreutils```
  * then to use the new dd, add this to your .bashrc or equivalent  
```$ export PATH="$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH"```
* and the main issue is that the oflag=direct argument isn't supported.  see below for details.

## Usage examples
```
$ ./iotest.py
usage: iotest.py [-h] [-c COUNT] [-s SIZE] [-k] [-nd] [-src SOURCE]
                 [--delay DELAY] [-v | -scr] [-d DESTINATION | -ro]

Do N write/read tests from/to /dev/zero to/from $target

optional arguments:
  -h, --help            show this help message and exit
  -c COUNT, --count COUNT
                        Number of test iterations. (default: 4)
  -s SIZE, --size SIZE  Payload per iteration in MB. (default: 150)
  -k, --keep            Dont delete test data upon completion (default: False)
  -nd, --nodirect       Dont use dd argument oflag=direct. Use with caution!
                        (default: False)
  -src SOURCE, --source SOURCE
                        Where to read data from. (default: /dev/zero)
  --delay DELAY         Delay between tests (in seconds) (default: 0)
  -v, --verbose         More verbose output, such as the dd return from each
                        test. (default: False)
  -scr, --script        Output values as CSV suitable to pipe to a log file
                        (default: False)
  -d DESTINATION, --destination DESTINATION
                        Where to write data to. (default: .)
  -ro, --readonly       Perform read test from --source, no write test
                        (default: False)
```

basic operation:

```
$ ./iotest.py -d .
Doing 4 write/read tests of 150 MB from/to /dev/zero to/from /home/hoolio/.PBJ4HUMF.dat
 1  2  3  4  OK!
write: min/avg/max/mdev is 275.0/281.25/284.0/3.63 MB/s in 0.56 seconds (average)
 1  2  3  4  OK!
read: min/avg/max/mdev is 258.0/262.0/268.0/3.74 MB/s in 0.6 seconds (average)
```

or the same, but using the long arguments and now in verbose mode:

```
$ ./iotest.py --destination . --verbose
Doing 4 write/read tests of 150 MB from/to /dev/zero to/from /home/hoolio/.YQGLBJTT.dat
..running iotest 1.3.1 on debian with dd version 8.23
writing with: dd if=/dev/zero of=/home/hoolio/.YQGLBJTT.dat bs=1M count=150 oflag=direct
..157286400 bytes (157 MB) copied, 0.545453 s, 288 MB/s
..157286400 bytes (157 MB) copied, 0.556732 s, 283 MB/s
..157286400 bytes (157 MB) copied, 0.550341 s, 286 MB/s
..157286400 bytes (157 MB) copied, 0.544084 s, 289 MB/s
write: min/avg/max/mdev is 283.0/286.5/289.0/2.29 MB/s in 0.55 seconds (average)
reading with with: dd if=/home/hoolio/.YQGLBJTT.dat of=/dev/null bs=1M count=150 iflag=direct
..157286400 bytes (157 MB) copied, 0.598659 s, 263 MB/s
..157286400 bytes (157 MB) copied, 0.605545 s, 260 MB/s
..157286400 bytes (157 MB) copied, 0.580498 s, 271 MB/s
..157286400 bytes (157 MB) copied, 0.580637 s, 271 MB/s
read: min/avg/max/mdev is 260.0/266.25/271.0/4.87 MB/s in 0.59 seconds (average)
```

In script mode, where output would be redirected to logfile.  
Note field scheme is tied to script version number, where the field scheme is documented further down.

```
$ ./iotest.py -d ~ --script
1.3.1,1508301105.0,144.6.224.56,extjess,write,/home/hoolio/.29WFEOX1.dat,150,279.0,291.0,304.0,9.41,0.54
1.3.1,1508301105.0,144.6.224.56,extjess,read,/home/hoolio/.29WFEOX1.dat,150,259.0,269.25,275.0,6.1,0.58
```

playing with count and size options

```
$ ./iotest.py -d ~ -c 15 -s 2
Doing 15 write/read tests of 2 MB from/to /dev/zero to/from /home/hoolio/.6LXIKR56.dat
 1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  OK!
write: min/avg/max/mdev is 186.0/369.73/522.0/100.69 MB/s in 0.01 seconds (average)
 1  2  3  4  5  6  7  8  9  10  11  12  13  14  15  OK!
read: min/avg/max/mdev is 231.0/567.13/937.0/197.31 MB/s in 0.0 seconds (average)
```

read only test, requires a source data file:

```
$ ./iotest.py --readonly --source .PRUN49T4.dat
Doing 4 read tests of 150 MB from .PRUN49T4.dat to /dev/null
 1  2  3  4  OK!
read: min/avg/max/mdev is 169.0/245.75/273.0/44.34 MB/s in 0.67 seconds (average)
```
## What's the issue with --nodirect?
iotest uses dd to do the read and write tests.  the important oflag=direct dd flag attempts to bypass system and disk caches and give you a more accurate representation of your speed.  you can opt to not use the oflag=direct dd argument by running iotest with --nodirect but the figures will be much faster than they ought to be.  in order to accurately get benchmarks in that case, you would need a source data set that was not easily cachable, noting the iotest default of /dev/zero is extremley cachable ;)

using oflag=direct (which is the default):

```
$ ./iotest.py -d .
Doing 4 write/read tests of 150 MB from/to /dev/zero to/from ./.O5KPXAPG.dat
 1  2  3  4  OK!
write: min/avg/max/mdev is 554.0/572.0/595.0/15.05 MB/s in 0.28 seconds (average)
 1  2  3  4  OK!
read: min/avg/max/mdev is 1331.2/1331.2/1331.2/0.0 MB/s in 0.12 seconds (average)
```
vs not using it via the --nodirect argument.  note the MUCH faster read and write speeds, because cache:

```
$ ./iotest.py -d . --nodirect
Doing 4 write/read tests of 150 MB from/to /dev/zero to/from ./.I4SWJCBR.dat
 1  2  3  4  OK!
write: min/avg/max/mdev is 455.0/730.75/882.0/163.4 MB/s in 0.23 seconds (average)
 1  2  3  4  OK!
read: min/avg/max/mdev is 4812.8/5862.4/7065.6/893.8 MB/s in 0.03 seconds (average)
```

if you were forced to use --nodirect for any testing any one of a number of computers, i'd suggest then using --nodirect on all of the computers.  it may also be time to look at using a proper disk benchmarking tool such as [bonnie++](https://en.wikipedia.org/wiki/Bonnie%2B%2B).

## field scheme for --script option:
v1.0 -> current:
```
this_script_version_number,unixtime,get_ip_address(),socket.gethostname(),test_prettyname,test_data_file,args.size,minimum,average,maximum,mdev,duration
```
