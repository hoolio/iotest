#!/usr/bin/python
#
# iotest.py
# Julius Roberts 2017
# https://github.com/hooliowobbits/iotest
#
# Test i/o using dd to do writes/reads from/to source to/from target path
#
# TO DO:
# add option to write from a random data source held in ramdrive
# Handle errors better when used with --script.  It shouldn't mess up logfiles
#
# VERSION HISTORY
# v1.3.3: addded support for --nodirect.  not sure why you'd want that though..
# v1.3.2: added option to delay N seconds between test iterations
# v1.3.1: tidied up arguments
# v1.3.0: added option to do read only check
# v1.2.0: removed numpy dependency
# v1.1.0: initial release

this_script_version_number="1.3.3"

import sys, os, subprocess, argparse, platform, socket
import time, random, string, math, datetime

dtime = datetime.datetime.now()
unixtime = time.mktime(dtime.timetuple())

DEVNULL = open(os.devnull, 'wb')
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

#-----------
# ARGPARSE
#-----------
parser = argparse.ArgumentParser(description='Do N write/read tests from/to /dev/zero to/from $target',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-c','--count', type=int, default=4, help='Number of test iterations.')
parser.add_argument('-s','--size', type=int, default=150, help='Payload per iteration in MB.')
parser.add_argument('-k','--keep', action='store_true', help='Dont delete test data upon completion', default=False)
parser.add_argument('-nd','--nodirect', action='store_true', help='Dont use dd argument oflag=direct.  Use with caution!', default=False)
parser.add_argument('-src','--source', type=str, default='/dev/zero', help='Where to read data from.')
parser.add_argument('--delay', type=int, help='Delay between tests (in seconds)', default=0)

group = parser.add_mutually_exclusive_group()
group.add_argument('-v','--verbose', action='store_true', help='More verbose output, such as the dd return from each test.', default=False)
group.add_argument('-scr','--script', action='store_true', help='Output values as CSV suitable to pipe to a log file', default=False)

group2 = parser.add_mutually_exclusive_group()
group2.add_argument('-d','--destination', type=str, help='Where to write data to.', default=".")
group2.add_argument('-ro','--readonly', action='store_true', help='Perform read test from --source, no write test', default=False)

# print help if no args given
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# ensure a valid source is specified when doing --readonly
#  i expect there's a argparse way to do this, but as yet i don't know what that is.
if args.readonly:
    if args.source == "/dev/zero":
        print "\nERROR: --readonly requires you to specify a --source."
        sys.exit(1)
    else:
        # make sure the specified path exists
        if not os.path.isfile(args.source):
            print "\nERROR: Unable to find %s" % args.source
            sys.exit(1)

#-----------
# BOOTSTRAP
#-----------
def random_string_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def touch(fname, times=None):
    with open(fname, 'a'):
        os.utime(fname, times)

if args.source: args.source = args.source.rstrip('/')
if args.destination:
    args.destination = args.destination.rstrip('/')
    test_data_file= args.destination + '/.' + random_string_generator() + '.iotest.dat'

    try:
        touch(test_data_file)
    except IOError as e:
        print "\nERROR: I/O error(%s): %s on %s" % (e.errno, e.strerror,test_data_file)
        sys.exit(1)

#-----------------
# DD VERSION CHECK
#-----------------
dd_version_check_command='dd --version | head -1'
try:
    p = subprocess.Popen(dd_version_check_command, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL, close_fds=True)
    output = p.stdout.read()
    output = output.split()
    dd_version = output[2]
except:
    print "\nERROR: Can't get dd version.  Is dd installed?"
    sys.exit()

#-----------
# DEF TEST
#-----------
def perform_test(test_name,test_prettyname):
    test_command=test_name + ' 2>&1 | grep copied'
    list_of_speeds_in_MBs = []
    list_of_times_in_sec = []

    for x in range(0,args.count):
        p = subprocess.Popen(test_command, shell=True, stdin=DEVNULL, stdout=subprocess.PIPE, stderr=DEVNULL, close_fds=True)
        output = p.stdout.read()

        # handle printing of output
        if not args.script:
            if args.verbose:
                print ".." + output.rstrip('\n')
            else:
                xplus1 = x + 1
                print " %s" % xplus1,
                if x == (args.count-1): print ' OK!'

        # parse output
        try:
            output = output.split()
            # 8.4 ..157286400 bytes (157 MB) copied, 0.648184 s, 243 MB/s
            if "8.4" in dd_version:
                duration = float(output[5])
                speed = float(output[7])
                unit = output[8]
            # 8.13  ..157286400 bytes (157 MB) copied, 0.621732 s, 253 MB/s
            elif "8.13" in dd_version:
                duration = float(output[5])
                speed = float(output[7])
                unit = output[8]
            # 8.23  ..157286400 bytes (157 MB) copied, 0.d85681 s, 184 MB/s
            elif "8.23" in dd_version:
                duration = float(output[5])
                speed = float(output[7])
                unit = output[8]
            # 8.25  ..157286400 bytes (157 MB, 150 MiB) copied, 0.120917 s, 1.3 GB/s
            elif "8.25" in dd_version:
                duration = float(output[7])
                speed = float(output[9])
                unit = output[10]
            # 8.26  ..157286400 bytes (157 MB, 150 MiB) copied, 2.29135 s, 68.6 MB/s
            elif "8.26" in dd_version:
                duration = float(output[7])
                speed = float(output[9])
                unit = output[10]
            # 8.28  ..157286400 bytes (157 MB, 150 MiB) copied, 0.210835 s, 746 MB/s
            elif "8.28" in dd_version:
                duration = float(output[7])
                speed = float(output[9])
                unit = output[10]
            else:
                print "\nSORRY: dd version %s is unsupported; contact Hoolio." % dd_version
                sys.exit()

            # unit conversion
            if unit == 'GB/s': speed = speed * 1024
            if unit == 'MB/s': speed = speed
            if unit == 'KB/s': speed = speed / 1024

        except:
            print "\nERROR: Unable to parse output from dd, try --verbose?"
            sys.exit()

        list_of_speeds_in_MBs.append(speed)
        list_of_times_in_sec.append(duration)

        if args.delay > 0:
            time.sleep(args.delay)

    # Calculate statistics, ping style
    #rtt min/avg/max/mdev = 0.024/0.026/0.028/0.002 ms
    average = round(sum(list_of_speeds_in_MBs) / len(list_of_speeds_in_MBs),2)
    minimum = min(list_of_speeds_in_MBs)
    maximum = max(list_of_speeds_in_MBs)

    def calculate_average(s): return sum(s) * 1.0 / len(s)
    variance=map(lambda x: (x - average)**2, list_of_speeds_in_MBs)
    mdev = round(math.sqrt(calculate_average(variance)),2)
    #import numpy
    #mdev_numpy= round(numpy.std(list_of_speeds_in_MBs),2)
    #print mdev_numpy

    duration=round(sum(list_of_times_in_sec) / len(list_of_times_in_sec),2)
    if not args.script:
        print "%s: min/avg/max/mdev is %s/%s/%s/%s MB/s in %s seconds (average)" % (test_prettyname,minimum,average,maximum,mdev,duration)
    else:
        print "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (this_script_version_number,unixtime,get_ip_address(),socket.gethostname(),test_prettyname,test_data_file,args.size,minimum,average,maximum,mdev,duration)

def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    return s.getsockname()[0]

#-----------
# RUN TESTS
#-----------
if args.readonly:
    if args.nodirect:
        read_test='dd if=%s of=/dev/null bs=1M count=%s' % (args.source,args.size)
        write_test='dd if=%s of=%s bs=1M count=%s' % (args.source,test_data_file,args.size)
    else:
        read_test='dd if=%s of=/dev/null bs=1M count=%s iflag=direct' % (args.source,args.size)
        write_test='dd if=%s of=%s bs=1M count=%s oflag=direct' % (args.source,test_data_file,args.size)

    if not args.script: print "Doing %s read tests of %s MB from %s to /dev/null" % (args.count,args.size,args.source)
    if args.verbose: print "..running iotest %s on %s with dd version %s" % (this_script_version_number,platform.linux_distribution()[0],dd_version)

    if args.verbose: print "reading with with: %s" % read_test
    perform_test(read_test,"read")

else:
    if args.nodirect:
        read_test='dd if=%s of=/dev/null bs=1M count=%s' % (test_data_file,args.size)
        write_test='dd if=%s of=%s bs=1M count=%s' % (args.source,test_data_file,args.size)
    else:
        read_test='dd if=%s of=/dev/null bs=1M count=%s iflag=direct' % (test_data_file,args.size)
        write_test='dd if=%s of=%s bs=1M count=%s oflag=direct' % (args.source,test_data_file,args.size)

    if not args.script: print "Doing %s write/read tests of %s MB from/to %s to/from %s" % (args.count,args.size,args.source,test_data_file)
    if args.verbose: print "..running iotest %s on %s with dd version %s" % (this_script_version_number,platform.linux_distribution()[0],dd_version)

    if args.verbose: print "writing with: %s" % write_test
    perform_test(write_test,"write")
    if args.verbose: print "reading with with: %s" % read_test
    perform_test(read_test,"read")

#-----------
# CLEANUP
#-----------
if not args.keep:
    os.remove(test_data_file)
else:
    if not args.script: print "..%s has been kept" % test_data_file
